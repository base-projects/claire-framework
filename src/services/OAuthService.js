import jwt from 'jsonwebtoken';
import randomstring from 'randomstring';
import {errors, jsonError, jsonSuccess, logger} from '../utils/system';
import {OAuthApp} from '../models/schema/OAuthApp';
import {OAuthToken} from '../models/schema/OAuthToken';
import {OAuthRequest} from '../models/schema/OAuthRequest';

class OAuthService {

  static async boot() {
    return jsonSuccess();
  }

  static async getOAuthApps({filter}) {
    return await OAuthApp.find(filter).lean();
  }

  static async getAccessToken({tokenData}) {

    let token = new OAuthToken(tokenData);
    token = await token.save();

    //-- generate token
    let tokenResult = await new Promise((resolve) => {
      jwt.sign(tokenData, getEnv('JWT_SECRET'), {
        expiresIn: Number(getEnv('JWT_EXPIRE_SEC'))
      }, (err, token) => {
        if (err) {
          return resolve(jsonError(errors.SYSTEM_ERROR));
        }
        return resolve(jsonSuccess(token));
      });
    });
    if (!tokenResult.success) return tokenResult;

    //-- generate refresh token
    let refreshTokenResult = await new Promise((resolve) => {
      jwt.sign({tokenId: token._id}, getEnv('JWT_SECRET'), {
        expiresIn: Number(getEnv('JWT_REFRESH_EXPIRE_SEC'))
      }, (err, token) => {
        if (err) {
          logger.error('refreshTokenResult', err);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        }
        return resolve(jsonSuccess(token));
      });
    });
    if (!refreshTokenResult.success) return refreshTokenResult;

    return jsonSuccess({
      token: tokenResult.result,
      tokenExpiration: Date.now() + Number(getEnv('JWT_EXPIRE_SEC')) * 1000,
      refreshToken: refreshTokenResult.result,
    });
  }

  static async createOAuthApp({userId, name, callbackUrls}) {

    let app = await OAuthApp.findOne({name});
    if (app)
      return jsonError(errors.DUPLICATED_ERROR);

    app = new OAuthApp({
      userId,
      name,
      appKey: randomstring.generate({length: getEnv('OAUTH_APP_KEY_LENGTH')}),
      callbackUrls: callbackUrls
    });
    app = await app.save();
    return jsonSuccess(app);
  }

  static async editOAuthApp({id, name, callbackUrls}) {
    let app = await OAuthApp.findById(id);
    if (!app)
      return jsonError(errors.NOT_FOUND_ERROR);

    let otherApp = await OAuthApp.findOne({name});
    if (otherApp && String(otherApp._id) !== String(id))
      return jsonError(errors.DUPLICATED_ERROR);

    app.name = name;
    app.callbackUrls = callbackUrls;
    await app.save();

    return jsonSuccess();
  }

  static async removeOAuthApp({id}) {
    let app = await OAuthApp.findById(id);
    if (!app)
      return jsonError(errors.NOT_FOUND_ERROR);
    await app.remove();

    return jsonSuccess();
  }

  static async createOAuthRequest({userId, appId, redirectUrl, scope, state}) {

    let oAuthApp = await OAuthApp.findById(appId).lean();
    if (!oAuthApp)
      return jsonError(errors.NOT_FOUND_ERROR);

    //-- verify redirect_url
    if (redirectUrl && oAuthApp.callbackUrls && oAuthApp.callbackUrls.indexOf(redirectUrl) < 0)
      return jsonError(errors.REDIRECT_URL_NOT_MATCHED);

    //-- create a request
    let request = new OAuthRequest({
      appId,
      userId,
      scope,
      state,
      callbackUrl: redirectUrl || oAuthApp.callbackUrls[0],
      expiredAt: Date.now() + Number(getEnv('OAUTH_REQUEST_EXPIRATION_MS'))
    });
    request = await request.save();

    return jsonSuccess(request);
  }

  static async grantOAuthRequest({id, decision}) {
    let request = await OAuthRequest.findById(id);
    if (!request)
      return jsonError(errors.NOT_FOUND_ERROR);

    //-- check for request expiration
    if (request.expiredAt.getTime() < Date.now()) {
      await request.remove();
      return jsonError(errors.NOT_FOUND_ERROR);
    }

    request.accepted = !!decision;
    await request.save();

    return jsonSuccess();
  }
}

export {OAuthService};
