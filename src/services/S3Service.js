import aws from 'aws-sdk';

class S3Service {

  static async boot() {
    aws.config.update({
      accessKeyId: getEnv('AWS_ACCESS_KEY_ID'),
      secretAccessKey: getEnv('AWS_ACCESS_KEY_SECRET'),
    });
    S3Service.s3 = new aws.S3();
  }

  static async uploadFileToS3({bucket, fileName, fileContent}) {
    return new Promise((resolve) => {
      S3Service.s3.putObject({
        Body: fileContent,
        Bucket: bucket,
        Key: fileName,
      }, (err) => {
        if (err) {
          log.error('uploadFileToS3', err);
          return resolve(jsonError(errors.SYSTEM_ERROR));
        }
        return resolve(jsonSuccess());
      })
    });
  }
}

export {S3Service};
