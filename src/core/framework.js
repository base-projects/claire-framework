import {OAuthController} from '../controllers/OAuthController';
import {UserController} from '../controllers/UserController';

import {UserService} from '../services/UserService';
import {OAuthService} from '../services/OAuthService';
import {S3Service} from '../services/S3Service';

import {s as userSchema} from '../models/schema/User';
import {s as authSchema} from '../models/schema/Auth';
import {s as roleSchema} from '../models/schema/Role';
import {s as userRoleSchema} from '../models/schema/UserRole';
import {s as rolePermissionSchema} from '../models/schema/RolePermission';
import {m as oAuthAppSchema} from '../models/schema/OAuthApp';
import {m as oAuthTokenSchema} from '../models/schema/OAuthToken';
import {m as oAuthRequestSchema} from '../models/schema/OAuthRequest';

const environments = ['LCL', 'PRO', 'STG', 'DEV'];
const controllers = {UserController, OAuthController};
const services = {UserService, OAuthService, S3Service};
const schemas = {
  userSchema,
  authSchema,
  userRoleSchema,
  rolePermissionSchema,
  roleSchema,
  oAuthAppSchema,
  oAuthRequestSchema,
  oAuthTokenSchema
};

export {
  environments, controllers, services, schemas,
};
