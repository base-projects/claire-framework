let User;
const s = (builder, Sequelize) => {
	User = builder.define('User', {
		id: {
			type: Sequelize.INTEGER,
			primaryKey: true,
			autoIncrement: true,
			allowNull: false,
		},
		username: {
			type: Sequelize.STRING,
			allowNull: false
		}
	}, {});
	User.associate = (models) => {
		User.hasMany(models['Auth'], {foreignKey: 'userId'}); //-- user instance then call the promise user.getAuths()
	};
	return User;
};

export {User, s};
