let OAuthRequest;
const m = (builder, Mongoose)=>{
	OAuthRequest = builder.model('OAuthRequest', new Mongoose.Schema({
		appId: {type: Mongoose.Schema.ObjectId, ref: 'OAuthApp', required: true},
		userId: {type: Number, required: true},
		scope: {type: String, required: true},
		state: {type: String},
		accepted: {type: Boolean, default: false},
		callbackUrl: {type: String},
		expiredAt: {type: Date, required: true, default: Date.now},
	}));
};

export {OAuthRequest, m};
