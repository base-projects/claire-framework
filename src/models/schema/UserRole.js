let UserRole;
const s = (builder, Sequelize) => {
	UserRole = builder.define('UserRole', {
		userId: {
			allowNull: false,
			type: Sequelize.INTEGER,
			primaryKey: true,
		},
		roleId: {
			allowNull: false,
			type: Sequelize.INTEGER,
			primaryKey: true,
		}
	}, {});
	UserRole.associate = (models) => {
		UserRole.belongsTo(models['User']); //-- user instance then call the promise user.getUser()
		UserRole.belongsTo(models['Role']); //-- user instance then call the promise user.getRole()
	};
	return UserRole;
};

export {UserRole, s};
