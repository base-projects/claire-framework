'use strict';

module.exports = {
  up: (queryInterface, Sequelize) => {
    let jobs = [];
    jobs.push(queryInterface.addColumn('RolePermissions', 'createdAt', {
      allowNull: false,
      type: Sequelize.DATE
    }));
    jobs.push(queryInterface.addColumn('RolePermissions', 'updatedAt', {
      allowNull: false,
      type: Sequelize.DATE
    }));
    return Promise.all(jobs);
  },

  down: (queryInterface) => {
    let jobs = [];
    jobs.push(queryInterface.removeColumn('RolePermissions', 'updatedAt'));
    jobs.push(queryInterface.removeColumn('RolePermissions', 'createdAt'));
    return Promise.all(jobs);
  }
};
